$(function() {
  // text-placeholder
  $('.input').click(function() {
    $('.input-placeholder').css('visibility', 'hidden');
  });

  $('.input').keyup(function() {
    if ($('.input').text() == '') {
      $('.input-placeholder').css('visibility', 'visible');
    } else {
      $('.input-placeholder').css('visibility', 'hidden');
    }
  });
  // // open-widget
  // $('.open-btn').click(function() {
  //   $('.widget-container-wrap.current').removeClass('close');
  //   $('.widget-container-wrap.current').addClass('open');
  //   $('.close-conv').addClass('open');
  //   $('.open-conv').addClass('close');
  // });
  // // close-widget
  // $('.close-btn').click(function() {
  //   $('.widget-container-wrap').removeClass('open');
  //   $('.widget-container-wrap').addClass('close');
  //   $('.close-conv').removeClass('open');
  //   $('.open-conv').removeClass('close');
  // });
  $('.close-mobile').click(function() {
    $('.widget-container-wrap.current').removeClass('open');
    $('.close-conv').removeClass('open');
    $('.open-conv').removeClass('close');
  });
  $('.open-conv-wrap').click(function() {
    $('.jetlink-launcher').toggleClass('active');
    $('.widget-container-wrap').toggleClass('open');
    $('.open-conv .open-text').toggleClass('close');
    $('.open-conv .jetlink-badge').toggleClass('close');
  });

  // back-btn
  $('.back-btn').click(function() {
    $('.widget-container-wrap.widget-2 .me').html('');
    $('.widget-container-wrap.widget-2').removeClass('close');
    $('.widget-container-wrap.widget-2').addClass('open');
    $('.widget-container-wrap.current').removeClass('open');
    $('.widget-container-wrap.current').addClass('close');
  });
  $('.settings .back-btn').click(function() {
    $('.widget-container-wrap.current').addClass('close');
    $('.widget-container-wrap.widget-3').addClass('close');
    $('.widget-container-wrap.widget-2').removeClass('close');
    $('.widget-container-wrap.widget-2').addClass('open');
  });
  $('.settings-btn').click(function() {
    $('.widget-container-wrap.widget-3 .me').html('');
    $('.widget-container-wrap.widget-2').addClass('close');
    $('.widget-container-wrap.current').addClass('close');
    $('.widget-container-wrap.widget-3').removeClass('close');
    $('.widget-container-wrap.widget-3').addClass('open');
  });
  //content-show-hide
  $('.new-conv').click(function() {
    $('.widget-container-wrap.widget-3 .me').html('');
    $('.widget-container-wrap.widget-2').addClass('close');
    $('.widget-container-wrap.widget-3').removeClass('close');
    $('.widget-container-wrap.widget-3').addClass('open');

  });
  //shortcout-btn
  $('.question-wrap .green-btn.short-btn').click(function() {
    var btnVal = $(this).val();
    $('<button class="green-btn">' + btnVal + '</button>').appendTo($('.me')).addClass('new');
    $('.widget-body').animate({
      scrollTop: $('.widget-body').prop('scrollHeight')
    }, 1000);
  });
  //tag-btn
  $('.tag-group button.tag').click(function() {
    var tagBtnVal = $(this).val();
    $('<button class="tag">' + tagBtnVal + '</button>').appendTo($('.me'));
    $('.widget-body').animate({
      scrollTop: $('.widget-body').prop('scrollHeight')
    }, 1000);
  });
  //send message
  var $messages = $('.messages-content'),
    d, h, m,
    i = 0;

  function insertMessage() {
    var msg = $('.emojionearea-editor').html();
    var emoji = $('.emojionearea-editor img').attr('src');
    if (msg.replace(/\s|&nbsp;/g, '').length == 0) {
      return false;
    }
    if (msg != '') {
      $('<div class="me-text">' + $('.emojionearea-editor').html() + '</div>').appendTo($('.me')).addClass('new');
    }
    $('.conv-start .welcome-message').addClass('animated slideOutUp').fadeOut(200);
    $('.widget-header.welcome-header').hide();

    $('.widget-header.started-conv').show();
    $('.message-input').val(null);
    $('.widget-body').animate({
      scrollTop: $('.widget-body').prop('scrollHeight')
    }, 1000);
  }

  $('.widget-footer').keypress(function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
      insertMessage();
      $('.emojionearea-editor').text('');
      return false;

    }
    event.stopPropagation();
  });

  $('.green-btn.rounded-btn.message-submit').click(function() {
    insertMessage();
    $('.emojionearea-editor').text('');
    $('.green-btn.rounded-btn.message-submit').hide();
    $('.blue-btn.rounded-btn.file-input').show();
  });
  // notified
  if ($('.form label.btn.btn-default.email').hasClass('active')) {
    $('input.text').blur(function() {
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      var textVal = $('input.text').val();
      if (testEmail.test(textVal) && textVal != ' ') {
        $('.submit-btn').click(function() {
          $('.notified .form').removeClass('has-error');
          $('.notified.message').show();
          $('.notified.home').hide();
          $('.confirmed-mail-field').show();
          $('#confirmed-mail').text(textVal);
        });
      } else {
        $('.notified .form').addClass('has-error');
        $('.error-message-sms').fadeOut();
        $('.error-message').fadeIn(300);
      }
    });
  };



  $('#mailText').keypress(function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
      console.log('mailText enter');
      $('.green-btn.submit-btn').click();
      return false;
    }
    event.stopPropagation();
  });

  $('.form label.btn.email').on('click', function(event) {
    console.log('mail click');
    $('.error-message-sms').hide();
    $('#mailText').val(' ');
    $('#mailText').show();
    $('.intl-tel-input.allow-dropdown').hide();
    $('#phone').hide();
    $('.notified .form').removeClass('has-error');
    $('.submit-btn').click(function() {
      $('.error-message-sms').hide();
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      var textVal = $('#mailText').val();
      if (testEmail.test(textVal) && textVal != '') {
        $('.submit-btn').click(function() {
          $('.notified .form').removeClass('has-error');
          $('.notified.message').show();
          $('.notified.home').hide();
          $('.confirmed-mail-field').show();
          $('#confirmed-mail').text(textVal);
        });
      } else {
        $('.notified .form').addClass('has-error');
        $('.error-message-sms').fadeOut();
        $('.error-message').fadeIn(300);
      }
    });
  });

  $('.form label.btn.sms').on('click', function(event) {
    console.log('sms click');
    $('.error-message').fadeOut(300);
    $('#mailText').val(' ');
    $('#mailText').hide();
    $('.notified .form').removeClass('has-error');
    $('#phone').show();
    $('.intl-tel-input.allow-dropdown').show();

    $('.submit-btn').click(function() {
      var isValid = $('#phone').intlTelInput('isValidNumber');
      var smsVal = $('#phone').intlTelInput('getNumber');
      if (isValid == false) {
        $('.notified .form').addClass('has-error');
        $('.error-message-sms').fadeIn(300);
      } else if (isValid == true) {
        $('.notified .form').addClass('has-error');
        $('.confirmed-sms-field').show();
        $('.notified.home').hide();
        $('.notified.message').show();
        $('#confirmed-sms').text(smsVal);
        $('.error-message-sms').fadeOut(300);
      }
    });
  });

  //components
  //owl carousel
  $('.owl-carousel').owlCarousel({
    stagePadding: 20,
    center: true,
    items: 1,
    margin: 10,
    dots: false,
    nav: true,
    navText: [
      '<i class=\'fa fa-angle-left fa-lg\'></i>',
      '<i class=\'fa fa-angle-right fa-lg\'></i>'
    ],
  });

  //emojipicker
  $('[id^=\'jetlink-emojionearea\']').emojioneArea({
    pickerPosition: 'top',
    tonesStyle: 'bullet',
    placeholder: 'Type your message',
    events: {
      keypress: function(editor, event) {
        $('.blue-btn.rounded-btn.file-input').hide();
        $('.widget-footer .green-btn.rounded-btn').show();
      },
      keyup: function(editor, event) {
        if ($('.emojionearea-editor').html().length == 0) {
          $('.widget-footer .green-btn.rounded-btn').hide();
          $('.blue-btn.rounded-btn.file-input').show();
        } else {
          $('.widget-footer .green-btn.rounded-btn').show();
          $('.blue-btn.rounded-btn.file-input').hide();
        }
      }
    }
  });
  //input-tel-mask
  $('#phone').intlTelInput();

  //attachment
  var imagesPreview = function(input, placeToInsertImagePreview) {

    if (input.files) {
      var filesAmount = input.files.length;
      for (i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = function(event) {
          $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
    $('.widget-body').animate({
      scrollTop: $('.widget-body').prop('scrollHeight')
    }, 1000);
  };

  $('#file-attachment').on('change', function() {
    var ext = $('#file-attachment').val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['jpg', 'bmp', 'png', 'tif', 'gif', 'jpeg', 'pdf', 'doc', 'txt', 'docx', 'xlsx', 'xltx', 'xltm', 'xls', 'mp4', 'mkv', 'avi', 'mov', 'flv', '3gp', 'mpeg', 'mp3', 'wav', 'rar', '7z', 'zip']) == -1) {
      var errorText = 'Supported File Type : \'jpg\' \'bmp\' \'png\' \'tif\' \'gif\' \'jpeg\' \'pdf\' \'doc\' \'txt\' \'docx\' \'xlsx\' \'xltx\' \'xltm\' \'xls\' \'mp4\' \'mkv\' \'avi\' \'mov\' \'flv\' \'3gp\' \'mpeg\' \'mp3\' \'wav\' \'rar\' \'7z\' \'zip\'';
      $('<div class="file-type-alert"><p>' + errorText + '</p></div>').appendTo($('.me'));
      $('.widget-body').animate({
        scrollTop: $('.widget-body').prop('scrollHeight')
      }, 1000);
    } else if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'bmp', 'tif']) != -1) {
      imagesPreview(this, $('<div class="attachment-preview"></div>').appendTo($('.me')));
    } else if ($.inArray(ext, ['pdf', 'doc', 'txt', 'docx', 'xlsx', 'xltx', 'xltm', 'xls', 'mp4', 'mkv', 'avi', 'mov', 'flv', '3gp', 'mpeg', 'mp3', 'wav', 'rar', '7z', 'zip']) != -1) {
      var fileName = $('#file-attachment').val().split('/').pop().split('\\').pop();
      $('<div class="file-name"><i class="fa fa fa-upload"></i>' + fileName + '</div>').appendTo($('.me'));

      $('.widget-body').animate({
        scrollTop: $('.widget-body').prop('scrollHeight')
      }, 1000);
    }
  });

  //upload file type control
});
